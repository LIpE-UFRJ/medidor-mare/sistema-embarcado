import os
import requests

class Auth:

    def __init__(self, auth_url, device, password):
        self.auth_url = auth_url
        self.device = device
        self.password = password
        self.__token = ''

    def authenticate(self):
        headers = {
            'Content-Type': 'application/json'
        }
        body = {
            'deviceName': self.device, 
            'password': self.password
        }

        r = requests.post(self.auth_url, headers=headers, json=body)
        r.raise_for_status()

        if r.status_code == 200:
            print(r.json())
            self.token = r.json()['token']
            return self.token
        else:
            self.token = ''
            raise Exception(f"Erro {r.status_code}: {r.reason}.")

    @property
    def token(self):
        if self.__token == '' and os.path.isfile('.token'):
            with open('.token') as file:
                self.__token = file.read()

        return self.__token

    @token.setter
    def token(self, token):
        with open('.token', 'w') as file:
            file.write(token)
        self.__token = token