import RPi.GPIO as GPIO
import numpy as np
import os
import requests
import signal
import time
from dotenv import load_dotenv

import auth
import sensors

load_dotenv()

def SIGINT_handler(signum, frame):
    print('\nSistema interrompido pelo usuário.')
    GPIO.cleanup()
    exit()

signal.signal(signal.SIGINT, SIGINT_handler)

GPIO.setmode(GPIO.BCM)
GPIO_TRIGGER = 18
GPIO_ECHO = 24

sensor = sensors.Ultrassonic_sensor(GPIO_TRIGGER, GPIO_ECHO)
auth_agent = auth.Auth(
    f'{os.environ["SERVER_URL"]}/device/authenticate',
    os.environ['DEVICE_NAME'],
    os.environ['DEVICE_PASSWORD']
)

resumed_masurements = []

while True:
    print('Realizando tomada de medições...')

    start_time = time.time()
    timestamp = time.time()
    rawDistances = []
    while timestamp - start_time < 10:
        timestamp = time.time()
        distance = sensor.get_distance()
        rawDistances.append({'timestamp':timestamp, 'value':distance})

    print('Resumindo medições...')

    distances = [dist['value'] for dist in rawDistances]
    distance = {}
    distance['timestamp'] = (timestamp + start_time) / 2
    distance['median'] = np.median(distances)
    distance['mean'] = np.mean(distances)
    distance['stddev'] = np.std(distances)
    distance['percentile_25'] = np.percentile(distances, 25)
    distance['percentile_75'] = np.percentile(distances, 75)
    distance['measurements'] = len(distances)
    
    resumed_masurements.append({'type': 'distance', 'data': distance})
    
    data = {
        'rawMasurements': {
            'distances': rawDistances
            },
        'resumedMasurements': resumed_masurements
        }

    print('Enviando medições...')
    
    if auth_agent.token == '':
        print('Dispositivo não autenticado. Fazendo autenticação...')
        try:
            auth_agent.authenticate()
            print('Dispositivo autenticado.')
        except BaseException as err:
            print(err)
            continue

    headers = {
        'Content-Type': 'application/json',
        'authorization': f'Bearer {auth_agent.token}'
    }

    r = requests.post(
        f'{os.environ["SERVER_URL"]}/device/masurement', 
        headers=headers, 
        json=data
        )

    if r.status_code == 200:
        print('Todas as medições foram enviadas.')
        resumed_masurements = []
    else:
        if r.status_code == 401:
            auth_agent.token = ''
        print(f'Erro {r.status_code}: {r.reason}')
        print(f'{len(resumed_masurements)} medições a serem enviadas')
